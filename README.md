# WSN - OrignalTracker

# Tracking d'orignal
*Léni Gauffier - Nathan Dalaine*
___
## Spécifications :
- La carte doit être embarquée sur l'animal.
- Elle doit permettre de localiser l'animal selon plusieurs niveaux de précision.
### Niveaux de précision :
1. Localisation par GPS, une fois par jour pour donner une approximation de la zone.
2. Localisation par triangulation grâce à une gateway sur hélicoptère.
3. Localisation précise grâce à la puissance du signal lors d'une recherche sur terre.

## Matériel nécessaire :
- Carte LoRa ExpLoRer
- Capteur GPS
- Hélicoptère
- Fusil hypodermique de sorte à tranquilliser l'orignal