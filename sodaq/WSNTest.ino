#include "TheThingsNetwork.h"
#include "CayenneLPP.h"

// Board Definitions
#define bleSerial Serial1
#define loraSerial Serial2
#define debugSerial SerialUSB
#define SERIAL_TIMEOUT 10000
enum state
{
  WHITE,
  RED,
  GREEN,
  BLUE,
  CYAN,
  ORANGE,
  PURPLE,
  OFF
}; // List of colours for the RGB LED
byte BUTTON_STATE;
byte BUTTON_STATE_OFF;

// LoRa Definitions and constants
const char *devAddr = "260112A3";
const char *nwkSKey = "4155A4EF649409FA20DBE5484EA281FF";
const char *appSKey = "63A493497E11DE3E46191A7A0B51972D";

const bool CNF = true;
const bool UNCNF = false;
const byte MyPort = 3;
byte Payload[51];
byte CNT = 0;                 // Counter for the main loop, to track packets while prototyping
#define freqPlan TTN_FP_EU868 // Replace with TTN_FP_EU868 or TTN_FP_US915
#define FSB 0                 // FSB 0 = enable all channels, 1-8 for private networks
#define SF 7                  // Initial SF

TheThingsNetwork ExpLoRer(loraSerial, debugSerial, freqPlan, SF, FSB); // Create an instance from TheThingsNetwork class
CayenneLPP CayenneRecord(51);                                          // Create an instance of the Cayenne Low Power Payload

void setup()
{
  pinMode(BUTTON, INPUT_PULLUP);
  BUTTON_STATE = digitalRead(BUTTON);
  BUTTON_STATE_OFF = BUTTON_STATE;
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_BLUE, OUTPUT);
  LED(RED); // Start with RED
  pinMode(TEMP_SENSOR, INPUT);
  analogReadResolution(10); //Set ADC resolution to 10 bits

  pinMode(LORA_RESET, OUTPUT); //Reset the LoRa module to a known state
  digitalWrite(LORA_RESET, LOW);
  delay(100);
  digitalWrite(LORA_RESET, HIGH);
  delay(1000); // Wait for RN2483 to reset
  LED(ORANGE); // Switch to ORANGE after reset

  loraSerial.begin(57600);
  debugSerial.begin(57600);

  // Wait a maximum of 10s for Serial Monitor
  while (!debugSerial && millis() < SERIAL_TIMEOUT)
    ;

  // Set callback for incoming messages
  ExpLoRer.onMessage(message);

  //Set up LoRa communications
  debugSerial.println("-- STATUS");
  ExpLoRer.showStatus();

  delay(1000);

  debugSerial.println("-- JOIN");

  if (ExpLoRer.personalize(devAddr, nwkSKey, appSKey))
    LED(GREEN); // Switch to GREEN if ABP succesful
  else
    LED(RED);

  delay(1000);

  //Hard-coded GPS position, just to create a map display on Cayenne
  CayenneRecord.reset();
  CayenneRecord.addGPS(1, 51.4141391, -0.9412872, 10); // Thames Valley Science Park, Shinfield, UK

  // Copy out the formatted record
  byte PayloadSize = CayenneRecord.copy(Payload);

  ExpLoRer.sendBytes(Payload, PayloadSize, MyPort, UNCNF);
  CayenneRecord.reset();
} // End of the setup() function

void loop()
{
  CNT++;
  debugSerial.println(CNT, DEC);

  LED(BLUE); // LED on while transmitting. Green for energy-efficient LoRa
  byte PayloadSize = CayenneRecord.copy(Payload);

  if (digitalRead(BUTTON) != BUTTON_STATE && digitalRead(BUTTON)!=BUTTON_STATE_OFF)
  {
    LED(GREEN); // Switch to GREEN
    // debugSerial.print("Button state = ");
    // debugSerial.println(BUTTON_STATE);
    CayenneRecord.addDigitalOutput(2, !BUTTON_STATE);
    byte response = ExpLoRer.sendBytes(Payload, PayloadSize, MyPort, UNCNF);
  }
  BUTTON_STATE = digitalRead(BUTTON);


  delay(100);

  CayenneRecord.reset(); // Clear the record buffer for the next loop
  LED(OFF);
  CayenneRecord.addGPS(1, 51.4141391, -0.9412872, 10);
} // End of loop() function

// Helper functions below here

float getTemperature()
{
  //10mV per C, 0C is 500mV
  float mVolts = (float)analogRead(TEMP_SENSOR) * 3300.0 / 1023.0;
  float temp = (mVolts - 500.0) / 10.0; // Gives value to 0.1degC
  return temp;
}

float getSound()
{
  long sum = 0;
  for (int i = 0; i < 32; i++)
  {
    sum += analogRead(A8);
  }

  sum >>= 5;

  debugSerial.println(sum);
  delay(10);
  return sum;
}

void LED(byte state)
{
  switch (state)
  {
  case WHITE:
    digitalWrite(LED_RED, LOW);
    digitalWrite(LED_GREEN, LOW);
    digitalWrite(LED_BLUE, LOW);
    break;
  case RED:
    digitalWrite(LED_RED, LOW);
    digitalWrite(LED_GREEN, HIGH);
    digitalWrite(LED_BLUE, HIGH);
    break;
  case ORANGE:
    digitalWrite(LED_RED, LOW);
    digitalWrite(LED_GREEN, LOW);
    digitalWrite(LED_BLUE, HIGH);
    break;
  case CYAN:
    digitalWrite(LED_RED, HIGH);
    digitalWrite(LED_GREEN, LOW);
    digitalWrite(LED_BLUE, LOW);
    break;
  case PURPLE:
    digitalWrite(LED_RED, LOW);
    digitalWrite(LED_GREEN, HIGH);
    digitalWrite(LED_BLUE, LOW);
    break;
  case BLUE:
    digitalWrite(LED_RED, HIGH);
    digitalWrite(LED_GREEN, HIGH);
    digitalWrite(LED_BLUE, LOW);
    break;
  case GREEN:
    digitalWrite(LED_RED, HIGH);
    digitalWrite(LED_GREEN, LOW);
    digitalWrite(LED_BLUE, HIGH);
    break;
  default:
    digitalWrite(LED_RED, HIGH);
    digitalWrite(LED_GREEN, HIGH);
    digitalWrite(LED_BLUE, HIGH);
    break;
  }
}

// Callback method for receiving downlink messages. Uses ExpLoRer.onMessage(message) in setup()
void message(const uint8_t *payload, size_t size, port_t port)
{
  debugSerial.println("-- MESSAGE");
  debugSerial.print("Received " + String(size) + " bytes on port " + String(port) + ":");

  for (int i = 0; i < size; i++)
  {
    debugSerial.print(" " + String(payload[i]));
  }

  debugSerial.println();
}
